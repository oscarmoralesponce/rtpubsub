#ifndef __DATASTRUCTURE_H__
#define __DATASTRUCTURE_H__

#include "FreeRTOS.h"
#include "task.h"
#include <arm_math.h>


//Milliseconds to OS Ticks
#define M2T(X) (portTickType)(X*(configTICK_RATE_HZ/1000.0))
#define F2T(X) ((portTickType)((configTICK_RATE_HZ/(X))))

#define MAX_SUB 20
#define MAX_PUB 20

#define NR_OF_ANCHORS 5

#define MPU_DATA  1
#define FLOW_DATA  2
#define DWM_DATA  3
#define ROBOT_DATA 4
#define STATE_DATA 5

#define MPU_UPDATE 1
#define MPU_RATE_HZ 1000/(float)MPU_UPDATE
#define MPU_UPDATE_DT 1/MPU_RATE_HZ
#define G 9.81

#define FLOW_SPEED 10
#define FLOW_SPEED_HZ 1000/(float)FLOW_SPEED
#define FLOW_SPEED_DT  1/(float)FLOW_SPEED_HZ

typedef struct
{
    float x,y,z;
} Point;

typedef struct
{
    Point acc;
    Point rawacc;
    Point gyro;
    Point mag;
} Mpu_Data;

typedef struct
{
    float deltax;
    float deltay;
} Flow_Data;


typedef struct
{
    float x,y, z;
} Position_Data;

typedef struct
{
    float distance;
    uint8_t error;
} Distance_Data;

typedef struct
{
    float mu_a;
    float mu_theta;
} Robot_Data;

typedef struct
{
    float x;
    float y;
    float xdot;
    float ydot;
    float theta;
} State_Data;



#endif
