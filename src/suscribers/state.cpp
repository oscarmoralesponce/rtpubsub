/**
* state of the robot. 
*
* @author  Oscar Morales-Ponce
* @version 0.1
* @since   04-08-2019 
* 
* It computes a Kalman filter to determine the position and speed of the robot. 
* Publishes:
*	STATE_DATA 
* Subscribes: 	
* 	MPU_DATA: provides the readings of the MPU Gyro and Acce
* 	FLOW_DATA: provides the reading of the movement on X and Y (FlowDeck)
* 	DWM_DATA: provides the reading of position x,y,z from the DWM
* 
*/

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>

#include <arm_math.h>
#include "pubsub.h"
#include "ekf.h"
#include "datastructures.h"


#define UNIT   2.0f
#define SQRT2  1.4142135623731f
#define DIAGONAL UNIT * SQRT2

static bool bInit = false;
static void kalmanTask(void* args);
static void stateTask(void* args);


TaskHandle_t stateHandle = NULL;
TaskHandle_t kalmanHandle = NULL;


uint8_t stateId;

#define FREQUENCY_KF   10  // milliseconds
#define DT	     FREQUENCY_KF/1000.0f

#define DWMSTDDEV 0.2
#define FLOWSTDDEV 0.1
#define MPUSTDDEV 0.01

#define POSVARIANCE 5.0f
#define ACCVARIANCE 10.0f
#define THETAVARIANCE 5.0f

#define UNCERTANTY 1000.0f



#define STATE_SIZE 5

#define X_STATE      0
#define Y_STATE      1
#define XDOT_STATE   2
#define YDOT_STATE   3
#define THETA_STATE  4

#define VALUE_SIZE 5

#define GPSX_VALUE  0
#define GPSY_VALUE  1
#define FLOWX_VALUE 2
#define FLOWY_VALUE 3
#define GYRO_VALUE  4


static QueueHandle_t flowQueue;
static QueueHandle_t dwmQueue;
static QueueHandle_t mpuGyroAccQueue;
static QueueHandle_t robotQueue;
static QueueHandle_t stateQueue;

static float mu_a = 0;
static float mu_theta = 0;

ekf_t ekf;


// Variables for the Kalman Filter
float32_t X[STATE_SIZE];
float32_t x[STATE_SIZE];
float32_t F[STATE_SIZE][STATE_SIZE];
float32_t P[STATE_SIZE][STATE_SIZE];
float32_t Q[STATE_SIZE][STATE_SIZE];
float32_t R[VALUE_SIZE][VALUE_SIZE];
float32_t Y[VALUE_SIZE];
float32_t Z[VALUE_SIZE];
float32_t H[VALUE_SIZE][STATE_SIZE];

/* 
* ekf_reset: Initialize the variable of the Kalman filter to reinitialize. 
* The function has been left of the kalman filter so it can be customize 
*/
void ekf_reset()
{ 
    uint8_t i;

    arm_fill_f32(0.0f, &F[0][0], STATE_SIZE*STATE_SIZE);
    arm_fill_f32(0.0f, &P[0][0], STATE_SIZE*STATE_SIZE);
    arm_fill_f32(0.0f, &H[0][0], VALUE_SIZE*STATE_SIZE);
    
    arm_fill_f32(0.0f, &Q[0][0], STATE_SIZE*STATE_SIZE);
    arm_fill_f32(0.0f, &x[0],    STATE_SIZE);
    arm_fill_f32(0.0f, &R[0][0], VALUE_SIZE*VALUE_SIZE);
    arm_fill_f32(0.0f, &Z[0],    VALUE_SIZE);
    arm_fill_f32(0.0f, &Y[0],    VALUE_SIZE);
        
    
    for (i=0; i<STATE_SIZE; i++)
    {
        F[i][i] = 1.0f;
    }
      
    H[GPSX_VALUE][X_STATE] = 1.0f;
    H[GPSY_VALUE][Y_STATE] = 1.0f;
    H[FLOWX_VALUE][XDOT_STATE] = 1.0f;   
    H[FLOWY_VALUE][YDOT_STATE] = 1.0f;    
    H[GYRO_VALUE][THETA_STATE] = 1.0f;     
    
    Q[X_STATE][X_STATE] = POSVARIANCE;
    Q[Y_STATE][Y_STATE] = POSVARIANCE;
    Q[XDOT_STATE][XDOT_STATE] = ACCVARIANCE;
    Q[YDOT_STATE][YDOT_STATE] = ACCVARIANCE;
    Q[THETA_STATE][THETA_STATE] = THETAVARIANCE;
    
    P[X_STATE][X_STATE] = 0.01f;
    P[Y_STATE][Y_STATE] = 0.01f;
    P[THETA_STATE][THETA_STATE] = 0.01f;
    P[XDOT_STATE][XDOT_STATE] = 0.01f;
    P[YDOT_STATE][YDOT_STATE] = 0.01f;
   
    R[GPSX_VALUE][GPSX_VALUE] = UNCERTANTY;
    R[GPSY_VALUE][GPSY_VALUE] = UNCERTANTY;
    R[FLOWX_VALUE][FLOWX_VALUE] = UNCERTANTY;
    R[FLOWY_VALUE][FLOWY_VALUE] = UNCERTANTY;
    R[GYRO_VALUE][GYRO_VALUE] = UNCERTANTY;
    
    X[X_STATE] = 0;
    X[Y_STATE] = 0;
    X[XDOT_STATE] = 0;
    X[YDOT_STATE] = 0;
    X[THETA_STATE] = 0;  
}


/* 
* ekf_attitude_init: Initialize the matrices of the kalman filter. 
*/
void ekf_attitude_init()
{    
    ekf_reset();
    arm_mat_init_f32(&ekf.H,  VALUE_SIZE, STATE_SIZE, &H[0][0]);
    arm_mat_init_f32(&ekf.F,  STATE_SIZE, STATE_SIZE, &F[0][0]);
    arm_mat_init_f32(&ekf.x,  STATE_SIZE, 1, &x[0]);	
    arm_mat_init_f32(&ekf.X,  STATE_SIZE, 1, &X[0]);
    arm_mat_init_f32(&ekf.P,  STATE_SIZE, STATE_SIZE, &P[0][0]);
    arm_mat_init_f32(&ekf.Q,  STATE_SIZE, STATE_SIZE, &Q[0][0]);
    arm_mat_init_f32(&ekf.R,  VALUE_SIZE, VALUE_SIZE, &R[0][0]);
    arm_mat_init_f32(&ekf.Y,  VALUE_SIZE, 1, &Y[0]);     
    ekf_init(&ekf, STATE_SIZE, VALUE_SIZE);
}


/* 
* ekf_attitude_update: Execute a step ot the kalman filter. 
*/
void ekf_attitude_update()
{
      X[X_STATE] = x[X_STATE] + DT*(x[XDOT_STATE]*x[XDOT_STATE])/2; 
      X[Y_STATE] = x[Y_STATE] + DT*(x[YDOT_STATE]*x[YDOT_STATE])/2; 
      X[XDOT_STATE] = x[XDOT_STATE] + DT*(mu_a*mu_a)/2 * cos(DT*(mu_theta*mu_theta)/2);
      X[YDOT_STATE] = x[YDOT_STATE] + DT*(mu_a*mu_a)/2 * sin(DT*(mu_theta*mu_theta)/2);
      X[THETA_STATE] = x[THETA_STATE] + DT*(mu_theta*mu_theta)/2;      
    
      F[X_STATE][XDOT_STATE] = DT;
      F[X_STATE][XDOT_STATE] = DT;
      
      Y[GPSX_VALUE] = Z[GPSX_VALUE] - X[X_STATE];
      Y[GPSY_VALUE] = Z[GPSY_VALUE] - X[Y_STATE];
      Y[FLOWX_VALUE] = Z[FLOWX_VALUE]  - X[XDOT_STATE];
      Y[FLOWY_VALUE] = Z[FLOWY_VALUE] - X[XDOT_STATE];  
      Y[GYRO_VALUE] = Z[GYRO_VALUE] - X[THETA_STATE];  

    
      ekf_update(&ekf);
}

/* 
* stateInit: Initialize the task of the state. It subscribes to
* ATTITUDE_DATA: provides the attitude of the robot
* MPU_DATA: provides the readings of the MPU Gyro and Acce
* FLOW_DATA: provides the reading of the movement on X and Y (FlowDeck)
* DWM_DATA: provides the reading of position x,y,z from the DWM
* The task register as publisher which publish the new state
*/
void stateInit()
{
    if (bInit == false)
    {
    	xTaskCreate(stateTask, "STATE", configMINIMAL_STACK_SIZE, NULL, 4, &stateHandle);
    	xTaskCreate(kalmanTask, "KALMAN", configMINIMAL_STACK_SIZE, NULL, 3, &kalmanHandle);

	    dwmQueue  = xQueueCreate(1, sizeof(Distance_Data)*NR_OF_ANCHORS);
	    flowQueue  = xQueueCreate(1, sizeof(Flow_Data));
	    mpuGyroAccQueue = xQueueCreate(1, sizeof(Mpu_Data));
        robotQueue  = xQueueCreate(1, sizeof(Robot_Data));
        stateQueue  = xQueueCreate(1, sizeof(State_Data));

    	
	    registerSubscriber(MPU_DATA, stateHandle, mpuGyroAccQueue);
      	registerSubscriber(DWM_DATA, stateHandle, dwmQueue);
    	registerSubscriber(FLOW_DATA, stateHandle, flowQueue);	
        registerSubscriber(ROBOT_DATA, stateHandle, robotQueue);	
        stateId = registerPublisher(STATE_DATA, sizeof(State_Data), stateQueue);
	    if (stateId > 0)
	       bInit = true;
    }
}

/* 
* stateTest: return true if the task was correctly initialized. 
*/
bool stateTest()
{
    return bInit;
}

/* 
* kalmanTask: It executes at FREQUENCY_KF the Kalman filter and publish 
* the output o
*/
void kalmanTask(void* args)
{
    uint32_t lastWakeTime = xTaskGetTickCount();
    
    while (true)
    {
       vTaskDelayUntil(&lastWakeTime, FREQUENCY_KF / portTICK_RATE_MS);
       ekf_attitude_update();
       if  (ekf_step(&ekf))
       {
           State_Data stateData;
           stateData.x = x[X_STATE];
           stateData.y = x[Y_STATE];
           stateData.xdot = x[XDOT_STATE];
           stateData.ydot = x[YDOT_STATE];
           stateData.theta = x[THETA_STATE];
           xQueueOverwrite(stateQueue, &stateData);
	       publish(stateId);
 	   } 	
        R[GPSX_VALUE][GPSX_VALUE] = UNCERTANTY;
        R[GPSY_VALUE][GPSY_VALUE] = UNCERTANTY;
        R[FLOWX_VALUE][FLOWX_VALUE] = UNCERTANTY;
        R[FLOWY_VALUE][FLOWY_VALUE] = UNCERTANTY; 
        R[GYRO_VALUE][GYRO_VALUE] = UNCERTANTY;       
        mu_a = 0;
        mu_theta = 0;
    }
}


/* 
* stateTask: it computes when the data is received  from the subscription
* MPU_DATA: acccumulate the accelerometer reading
* FLOW_DATA: computes the actual distance based on the pixel movements
* DWM_DATA: Execute a triangulation
*/
void stateTask(void* args)
{
    uint32_t publisherId;

    while (true)	
    {
      if (xTaskNotifyWait(0xffffffff, 0xffffffff, &publisherId, portMAX_DELAY) == pdTRUE)
      {
	     switch (publisherId)
	     {
            case ROBOT_DATA:
	        {
		      Robot_Data robotData;
		      if (xQueueReceive(robotQueue, &robotData, ( TickType_t ) 0 )  == pdTRUE)
		      {
		          mu_theta += robotData.mu_theta;
                  mu_a += robotData.mu_a;
		      }
	          break;
            }
	        case MPU_DATA:
	        {
		      Mpu_Data gyroAcc;
		      if (xQueueReceive(mpuGyroAccQueue, &gyroAcc, ( TickType_t ) 0 )  == pdTRUE)
		      {
		          Z[GYRO_VALUE] += gyroAcc.gyro.z;
                  R[GYRO_VALUE][GYRO_VALUE] = 0.01;
		      }
	          break;
            }
	        case FLOW_DATA:
	        {  
     	       Flow_Data flowData;
		       if (xQueueReceive(flowQueue, &flowData, ( TickType_t ) 0 )  == pdTRUE)
		       {
		          Z[FLOWX_VALUE] = flowData.deltax * -0.01686;
		          Z[FLOWY_VALUE] = flowData.deltay * -0.01686;
		          R[FLOWX_VALUE][FLOWX_VALUE] = DWMSTDDEV;
		          R[FLOWY_VALUE][FLOWY_VALUE] = DWMSTDDEV;
		       }
                break;
	        }
	      	case DWM_DATA:
	        {
                Distance_Data distanceData[NR_OF_ANCHORS+1];
                float d[NR_OF_ANCHORS];
                float p[6], b[6];
                float error = 0.0f;

                const float rsquare = DIAGONAL * DIAGONAL;
                const float r2 = 2*DIAGONAL;
                int i;

                if (xQueueReceive(dwmQueue, distanceData, ( TickType_t ) 0 ) == pdTRUE)
                {
                for (i=0; i<NR_OF_ANCHORS; i++)
                {
                  if (distanceData[i].distance > 3 || distanceData[i].distance < -1)
                    break;
                  d[i] = distanceData[i].distance * distanceData[i].distance;
                  error += distanceData[i].error / 40.0f;
                }
                if (!(d[0] == 0 || d[1] == 0 || d[2] == 0 || d[3] == 0))
                {
                  p[0] =  (rsquare + d[0] - d[1])/r2;
                  p[1] =  (rsquare + d[0] - d[2])/r2;
                  p[2] =  (rsquare + d[0] - d[3])/r2;
                  p[3] =  (rsquare + d[1] - d[2])/r2;
                  p[4] =  (rsquare + d[1] - d[3])/r2;
                  p[5] =  (rsquare + d[2] - d[3])/r2;

                  b[0] = p[0] * SQRT2;
                  b[1] = p[1] * SQRT2;
                  b[2] = p[2] * SQRT2;
                  b[3] = UNIT - p[3] * SQRT2;
           //     b[4] = DIAGONAL - p[4] * SQRT2;
                  b[5] = UNIT - p[5] * SQRT2;

                  Z[GPSX_VALUE] = (b[0] + b[5])/2.0f;
                  Z[GPSY_VALUE] = (b[0] - b[5])/2.0f;

                  R[GPSX_VALUE][GPSX_VALUE] = error; // DWMSTDDEV / error;
                  R[GPSY_VALUE][GPSY_VALUE] = error; // DWMSTDDEV / error;
                }
		      }
	          break;
        }
         }
  }
    }
}
