#include <FreeRTOS.h>
#include <task.h>
#include <SPI.h>
#include <pubsub.h>
#include <mpu.h>
#include <flow.h>
#include <dwm.h>
#include <state.h>


SemaphoreHandle_t spiSemaphore;





int main() {
    uint8_t errorCode;
    
    SPI.begin();        
    Serial.begin(115200);
    spiSemaphore = xSemaphoreCreateMutex();

    pubsubInit();    
    mpuInit();  
    flowInit();
    dwmInit();
    stateInit();
  
    if (!pubsubTest())
    {
       errorCode = 1;
    }
    
    if (!mpuTest())
    {
      errorCode |= 1 << 1;
    }
    if (!flowTest())
    {
        errorCode |= 1 << 2;    
    }
    if (!dwmTest())
    {
	   errorCode |= 1 << 3;
    }
    if (!stateTest())
    {
       errorCode |= 1 << 4;
    } 
   
    // start scheduler, main should stop functioning here
    vTaskStartScheduler();

    for(;;);

    return 0;
}
